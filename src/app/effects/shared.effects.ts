import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { filter, map, switchMap, tap } from "rxjs/operators";
import {
  setErrorMsg,
  showErrorMsg,
  toggleSpinner,
} from "../actions/shared.actions";
import { SharedUtilService } from "../services/shared-util.service";

@Injectable()
export class SharedEffects {
  constructor(
    private _actions$: Actions,
    private _sharedUtilSvc: SharedUtilService
  ) {}

  setErrorMsg$ = createEffect(() =>
    this._actions$.pipe(
      ofType(setErrorMsg),
      filter((action) => !!action.errorMsg),
      map(() => showErrorMsg())
    )
  );

  showErrorMsg$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(showErrorMsg),
        tap(() => {
          this._sharedUtilSvc.displayErrorMessage();
        })
      ),
    { dispatch: false }
  );

  showSpinner$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(toggleSpinner),
        filter((action) => action.isShowSpinner),
        tap(() => this._sharedUtilSvc.displayLoadingSpinner())
      ),
    { dispatch: false }
  );

  dismissSpinner$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(toggleSpinner),
        filter((action) => !action.isShowSpinner),
        tap(() => this._sharedUtilSvc.dismissLoadingSpinner())
      ),
    { dispatch: false }
  );
}
