import { Injectable } from "@angular/core";
import { interval, Observable, throwError } from "rxjs";
import { catchError, map, take } from "rxjs/operators";
import { PersonModel } from "../models/person-model";

@Injectable({
  providedIn: "root",
})
export class PeopleService {
  constructor() {}

  private mockPeopleList = [
    {
      firstName: "John",
      lastName: "Doe",
      age: "21",
      workTitle: "Wanna be Signer",
    },
    { firstName: "Jane", lastName: "Doe", age: "22", workTitle: "Signer" },
    { firstName: "Bob", lastName: "Barker", age: "80", workTitle: "TV Host" },
    {
      firstName: "John",
      lastName: "Doe",
      age: "21",
      workTitle: "Wanna be Signer",
    },
  ];

  getPeople(): Observable<PersonModel[]> {
    return interval(2000).pipe(
      take(1),
      map(() => this.mockPeopleList),
      map((mockResp) => {
        return mockResp.map((mockPerson, index) => {
          return {
            id: index,
            firstName: mockPerson.firstName,
            lastName: mockPerson.lastName,
            age: +mockPerson.age,
            jobTitle: mockPerson.workTitle,
          };
        });
      }),
      catchError((err) => throwError(`${err}`))
    );
  }
}
