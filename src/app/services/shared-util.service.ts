import { Overlay, OverlayRef } from "@angular/cdk/overlay";
import { ComponentPortal } from "@angular/cdk/portal";
import { ComponentFactoryResolver, Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { take } from "rxjs/operators";
import { setErrorMsg } from "../actions/shared.actions";
import { LoadingSpinnerComponent } from "../components/loading-spinner/loading-spinner.component";
import { SharedState } from "../reducers/shared.reducer";
import { getErrorMsg } from "../reducers/shared.selectors";

@Injectable({ providedIn: "root" })
export class SharedUtilService {
  private _overlayRef: OverlayRef;

  constructor(
    private _store: Store<SharedState>,
    private _overlay: Overlay,
    private _componentResolver: ComponentFactoryResolver
  ) {}

  displayErrorMessage(): void {
    this._store
      .select(getErrorMsg)
      .pipe(take(1))
      .subscribe((errorMsg) => {
        alert(errorMsg);
        this._store.dispatch(setErrorMsg({ errorMsg: undefined }));
      });
  }

  displayLoadingSpinner(): void {
    if (!this._overlayRef) {
      const factory = this._componentResolver.resolveComponentFactory(
        LoadingSpinnerComponent
      );
      this._overlayRef = this._overlay.create({
        hasBackdrop: true,
        positionStrategy: this._overlay
          .position()
          .global()
          .centerHorizontally()
          .centerVertically(),
      });
      const portal = new ComponentPortal(factory.componentType);
      this._overlayRef.attach<LoadingSpinnerComponent>(portal);
    }
  }

  dismissLoadingSpinner(): void {
    this._overlayRef?.detach();
    this._overlayRef = undefined;
  }
}
