import { createReducer, on } from "@ngrx/store";
import { AppState } from ".";
import { setErrorMsg } from "../actions/shared.actions";

export interface SharedState extends AppState {
  errorMsg: string;
}

const initialState: SharedState = {
  errorMsg: undefined,
};

export const sharedReducer = createReducer(
  initialState,
  on(setErrorMsg, (state, { errorMsg }) => ({ ...state, errorMsg }))
);
