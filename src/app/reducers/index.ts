import { ActionReducerMap, MetaReducer } from "@ngrx/store";
import { environment } from "../../environments/environment";
import { sharedReducer } from "./shared.reducer";

export interface AppState {}

export const reducers: ActionReducerMap<AppState> = {
  app: {},
  shared: sharedReducer,
};

export const metaReducers: MetaReducer<AppState>[] = !environment.production
  ? []
  : [];
