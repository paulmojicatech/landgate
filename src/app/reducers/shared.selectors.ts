import { createFeatureSelector, createSelector } from "@ngrx/store";
import { SharedState } from "./shared.reducer";

const sharedStateSelector = createFeatureSelector<SharedState>("shared");

export const getErrorMsg = createSelector(
  sharedStateSelector,
  (state) => state.errorMsg
);
