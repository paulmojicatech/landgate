import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { MatTableModule } from "@angular/material/table";
import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";
import { PeopleEffects } from "./ngrx/effects/people.effects";
import * as fromPeople from "./ngrx/reducers/people.reducer";
import { PeopleNgrxRoutingModule } from "./people-ngrx-routing.module";

@NgModule({
  imports: [
    StoreModule.forFeature(fromPeople.peopleFeatureKey, fromPeople.reducer),
    EffectsModule.forFeature([PeopleEffects]),
    MatTableModule,
    PeopleNgrxRoutingModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class PeopleNgrxModule {}
