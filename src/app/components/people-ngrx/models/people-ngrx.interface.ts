import { PersonModel } from "src/app/models/person-model";

export interface PersonNgrxViewModel {
  people: PersonModel[];
  columns: string[];
}
