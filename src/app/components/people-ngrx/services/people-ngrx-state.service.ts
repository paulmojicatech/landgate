import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { BehaviorSubject, Observable, Subject } from "rxjs";
import { map, tap } from "rxjs/operators";
import { PersonModel } from "src/app/models/person-model";
import { PersonNgrxViewModel } from "../models/people-ngrx.interface";
import { loadPeoples, updatePerson } from "../ngrx/actions/people.actions";
import { State } from "../ngrx/reducers/people.reducer";
import { getPeople } from "../ngrx/selectors/people.selectors";

@Injectable()
export class PeopleNgrxStateService {
  readonly INITIAL_STATE: PersonNgrxViewModel = {
    people: undefined,
    columns: undefined,
  };

  private _viewModelSub$ = new BehaviorSubject<PersonNgrxViewModel>(
    this.INITIAL_STATE
  );
  viewModel$ = this._viewModelSub$.asObservable();

  constructor(private _store: Store<State>) {}

  getViewModel(): Observable<PersonNgrxViewModel> {
    this._store.dispatch(loadPeoples());
    return this._store.select(getPeople).pipe(
      map((people) => {
        return {
          people,
          columns: !!people?.length
            ? Object.keys(people[0]).filter((key) => key !== "id")
            : [],
        };
      }),
      tap((vm) => this._viewModelSub$.next(vm))
    );
  }

  updatePerson(updatedPerson: PersonModel): void {
    this._store.dispatch(updatePerson({ updatedPerson }));
  }
}
