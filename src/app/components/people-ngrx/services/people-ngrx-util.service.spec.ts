import { TestBed } from '@angular/core/testing';

import { PeopleNgrxUtilService } from './people-ngrx-util.service';

describe('PeopleNgrxUtilService', () => {
  let service: PeopleNgrxUtilService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PeopleNgrxUtilService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
