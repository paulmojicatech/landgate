import { TestBed } from '@angular/core/testing';

import { PeopleNgrxStateService } from './people-ngrx-state.service';

describe('PeopleNgrxStateService', () => {
  let service: PeopleNgrxStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PeopleNgrxStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
