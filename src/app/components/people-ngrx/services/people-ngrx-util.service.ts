import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { take } from "rxjs/operators";
import * as fromPeople from "../ngrx/reducers/people.reducer";
import { getIsLoaded } from "../ngrx/selectors/people.selectors";
@Injectable({
  providedIn: "root",
})
export class PeopleNgrxUtilService {
  constructor(private _store: Store<fromPeople.State>) {}

  getIsPeopleLoadedInState(): Observable<boolean> {
    return this._store.select(getIsLoaded).pipe(take(1));
  }
}
