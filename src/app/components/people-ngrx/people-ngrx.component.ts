import { Component, OnInit, ViewChild } from "@angular/core";
import { Observable } from "rxjs";
import { PeopleNgrxStateService } from "./services/people-ngrx-state.service";
import { PersonNgrxViewModel } from "./models/people-ngrx.interface";
import { MatDrawer } from "@angular/material/sidenav";
import { PersonModel } from "src/app/models/person-model";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-people-component",
  templateUrl: "./people-ngrx.component.html",
  styleUrls: ["./people-ngrx.component.scss"],
  providers: [PeopleNgrxStateService],
})
export class PeopleNgrxComponent implements OnInit {
  @ViewChild("drawer")
  drawer: MatDrawer;

  viewModel$: Observable<PersonNgrxViewModel>;
  editPersonForm: FormGroup;

  constructor(
    private _peopleNgrxStateSvc: PeopleNgrxStateService,
    private _fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.viewModel$ = this._peopleNgrxStateSvc.getViewModel();
    this.editPersonForm = this._fb.group({
      id: [null],
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      age: [null, Validators.required],
      jobTitle: [null, Validators.required],
    });
  }

  handleRowSelected(selectedPerson: PersonModel): void {
    const { id, firstName, lastName, age, jobTitle } = selectedPerson;
    this.editPersonForm.patchValue({
      id,
      firstName,
      lastName,
      age,
      jobTitle,
    });
    this.drawer.toggle();
  }

  handleUpdatePerson(): void {
    const updatedPerson = { ...this.editPersonForm.value };
    this._peopleNgrxStateSvc.updatePerson(updatedPerson);
    this.drawer.toggle();
  }
}
