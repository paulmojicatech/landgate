import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Route, RouterModule } from "@angular/router";
import { PeopleNgrxComponent } from "./people-ngrx.component";
import { MatTableModule } from "@angular/material/table";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";

const routes: Route[] = [
  {
    path: "",
    pathMatch: "full",
    component: PeopleNgrxComponent,
  },
];

@NgModule({
  declarations: [PeopleNgrxComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatTableModule,
    MatSidenavModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    ReactiveFormsModule,
  ],
  exports: [RouterModule, PeopleNgrxComponent, MatTableModule],
})
export class PeopleNgrxRoutingModule {}
