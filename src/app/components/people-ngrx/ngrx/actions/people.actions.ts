import { createAction, props } from "@ngrx/store";
import { PersonModel } from "../../../../models/person-model";

// This is a request
export const loadPeoples = createAction("[People] Load Peoples");

export const loadPeoplesSuccecss = createAction(
  "[People] Load Peoples Success",
  props<{ people: PersonModel[] }>()
);

export const loadPeoplesFail = createAction(
  "[People] Load Peoples Fail",
  props<{ errorMsg: string }>()
);

export const updatePerson = createAction(
  "[People] Update Person",
  props<{ updatedPerson: PersonModel }>()
);
