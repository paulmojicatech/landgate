import { Action, createReducer, on } from "@ngrx/store";
import { PersonModel } from "src/app/models/person-model";
import * as PeopleActions from "../actions/people.actions";

export const peopleFeatureKey = "people";

// TODO: Need to add People to the store and initialize it.
export interface State {
  isLoaded: boolean;
  people: PersonModel[];
}

export const initialState: State = {
  isLoaded: false,
  people: [],
};

export const reducer = createReducer(
  initialState,
  on(PeopleActions.loadPeoplesSuccecss, (state, { people }) => ({
    ...state,
    people,
    isLoaded: true,
  })),
  on(PeopleActions.updatePerson, (state, { updatedPerson }) => {
    const updatedList = state.people.map((person) => {
      if (person.id === updatedPerson.id) {
        return updatedPerson;
      }
      return person;
    });
    return { ...state, people: updatedList };
  })
);
