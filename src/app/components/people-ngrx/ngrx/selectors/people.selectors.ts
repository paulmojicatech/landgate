import { createFeatureSelector, createSelector } from "@ngrx/store";
import * as fromPeople from "../reducers/people.reducer";

const selectPeopleState = createFeatureSelector<fromPeople.State>(
  fromPeople.peopleFeatureKey
);

export const getIsLoaded = createSelector(
  selectPeopleState,
  (state) => state.isLoaded
);

export const getPeople = createSelector(
  selectPeopleState,
  (state) => state.people
);
