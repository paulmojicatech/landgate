import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";

import {
  catchError,
  concatMap,
  filter,
  map,
  mergeMap,
  switchMap,
} from "rxjs/operators";
import { EMPTY, of, throwError } from "rxjs";

import * as PeopleActions from "../actions/people.actions";
import { PeopleService } from "src/app/services/people.service";
import { setErrorMsg, toggleSpinner } from "src/app/actions/shared.actions";
import { PeopleNgrxUtilService } from "../../services/people-ngrx-util.service";

@Injectable()
export class PeopleEffects {
  constructor(
    private actions$: Actions,
    private _peopleSvc: PeopleService,
    private _peopleUtilSvc: PeopleNgrxUtilService
  ) {}

  loadPeoples$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PeopleActions.loadPeoples),
      switchMap(() => this._peopleUtilSvc.getIsPeopleLoadedInState()),
      filter((isAlreadyLoaded) => !isAlreadyLoaded),
      mergeMap(() =>
        this._peopleSvc.getPeople().pipe(
          map((people) => PeopleActions.loadPeoplesSuccecss({ people })),
          catchError((err) => [
            PeopleActions.loadPeoplesFail({ errorMsg: `${err}` }),
          ])
        )
      )
    );
  });

  loadPeopleShowSpinner$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PeopleActions.loadPeoples),
      switchMap(() => this._peopleUtilSvc.getIsPeopleLoadedInState()),
      filter((isAlreadyLoaded) => !isAlreadyLoaded),
      map(() => toggleSpinner({ isShowSpinner: true }))
    )
  );

  loadPeopleSuccessHideSpinner$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PeopleActions.loadPeoplesSuccecss),
      map(() => toggleSpinner({ isShowSpinner: false }))
    )
  );

  loadPeopleFailShowErr$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PeopleActions.loadPeoplesFail),
      map((action) => setErrorMsg({ errorMsg: action.errorMsg }))
    )
  );

  loadPeopleFailHideSpinner$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PeopleActions.loadPeoplesFail),
      map(() => toggleSpinner({ isShowSpinner: false }))
    )
  );
}
