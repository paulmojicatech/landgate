export interface PersonModel {
  id: number;
  firstName: string;
  lastName: string;
  age: number;
  jobTitle: string;
}
