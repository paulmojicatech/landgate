import { createAction, props } from "@ngrx/store";

export const setErrorMsg = createAction(
  "[Shared] Set Error Msg",
  props<{ errorMsg: string }>()
);

export const showErrorMsg = createAction("[Shared] Show Error Modal");

export const toggleSpinner = createAction(
  "[Shared] Toggle Spinner Visibility",
  props<{ isShowSpinner: boolean }>()
);
